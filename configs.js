var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE              : 'configs_overwrite.js',
	MONGO_HOST                          : "127.0.0.1",
	MONGO_PORT                          : "27019",
	MONGO_DB_NAME 											: "",
	MONGO_UNAME													: "",
	MONGO_PASS													: "",
	AWS_ACCESS_KEY											: "",
	AWS_SECRET_KEY											: "",
	AWS_REGION 													: "ap-southeast-1", 
	AWS_OUTPUT            							: "table",
	AMAZON_EXPIRY_TIME                  : 60*60,
	BACKUP_EXPIRY_TIME									: 900,
	BUCKET_NAME_DB_BACKUP               : "",
	ENCRYPTION_KEY                      : "",
	ENCRYPTION_SALT                     : "",
	ALGORITHM                           : "",
	MONGO_PARTIAL_DUMP_FILE_DIR         : "",
	MONGO_PARTIAL_DUMP_FILENAME         : "",
	MONGO_PARTIAL_DUMP_FILE_FULL_PATH   : "",
	MONGO_FULL_DUMP_FILE_DIR            : "",
	MONGO_FULL_DUMP_FILENAME            : "",
	MONGO_FULL_DUMP_FILE_FULL_PATH      : "",
	MONGO_RESTORE_FILE_DIR              : "",
	MONGO_RESTORE_FILE_NAME             : "",
	IDENTITY_FULL_FILE_PATH             : "",
	REDIS_SERVER_SSH_USER               : "",
	REDIS_SERVER_SSH_HOST               : "",
	REDIS_SERVER_REMOTE_DUMP_FILE_PATH  : "",
	REDIS_SERVER_DUMP_DIR               : ""

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any config key');
}

module.exports = configs;