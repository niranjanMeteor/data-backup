var fs                                = require('fs');

var configs = {

	CONFIGS_OVERWRITE_FILE              : "configs_overwrite.js",
	ANALYTICS_SMTP_HOST									: '',
	ANALYTICS_SMTP_PORT									: 0,
	ANALYTICS_SMTP_USERNAME							: '',
	ANALYTICS_SMTP_PASSWORD							: '',
	ANALYTICS_FROM											: '',
	ANALYTICS_TO 												: '',
	TECH_TO															: ''

};

var overwriteConfigFulFileName  = __dirname+'/'+configs.CONFIGS_OVERWRITE_FILE;
if(fs.existsSync(overwriteConfigFulFileName)){
	var overwriteConfig  =  require(overwriteConfigFulFileName);
	for (var key in overwriteConfig){
		configs[key] = overwriteConfig[key];
	}
} else {
	console.log('No Overwrite Configs File Found to overwrite any Application configs key');
}

module.exports = configs;