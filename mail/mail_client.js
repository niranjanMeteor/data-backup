var nodemailer 				= require('nodemailer');
var smtpTransport 		= require('nodemailer-smtp-transport');
var ip                = require('ip');
var os                = require("os");
var configs					  = require('./configs.js');

var transporter = nodemailer.createTransport(smtpTransport({
  host		: configs.ANALYTICS_SMTP_HOST,
  port		: configs.ANALYTICS_SMTP_PORT,
  secure 		: true,
  auth		: {
    user: configs.ANALYTICS_SMTP_USERNAME,
    pass: configs.ANALYTICS_SMTP_PASSWORD
  }
}));

var myIpAddres  = ip.address(); 
var hostname    = os.hostname();

exports.send = function (message) {
  var html_string = '<h4>'+  Date() +' : '+message+' </h4>';
  var mailOptions = {
    from    : configs.TECH_FROM,
    to      : configs.TECH_TO,
    subject : myIpAddres+' : '+hostname+' : BACK-UP Service has Crashed, '+Date(),
    html    : html_string
  };
  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      console.trace(error);
    }
    return;
  });
};