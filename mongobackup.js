var spawn = require('child_process').spawn;

var AWS                 = require('aws-sdk');
var fs                  = require('fs');
var backupConfigs       = require('./configs.js');
var crypto              = require('crypto');
var MailClient          = require('./mail/mail_client.js');

AWS.config.update({
  accessKeyId     			: backupConfigs.AWS_ACCESS_KEY,
  secretAccessKey 			: backupConfigs.AWS_SECRET_KEY,
  "region"        			: backupConfigs.AWS_REGION,
  "output"        			: backupConfigs.AWS_OUTPUT 
});
var s3                  = new AWS.S3();

var dbHost								= backupConfigs.MONGO_HOST;
var dbPort								= backupConfigs.MONGO_PORT;
var dbName								= backupConfigs.MONGO_DB_NAME;
var dbUser								= backupConfigs.MONGO_UNAME;
var dbPwd								  = backupConfigs.MONGO_PASS;

var current_date          = new Date();
var file_date_ext         = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate()+'_'+current_date.getHours()+'_'+current_date.getMinutes();
var dbDumpFilePath,out_encrypted_file,out_encrypted_file_path;
var args                  = ['--host='+dbHost,
                              '--port='+dbPort,
                              '--db='+dbName,
                              '--username='+dbUser,
                              '--password='+dbPwd,
                              '--quiet',
                              '--gzip'
                            ];

if(process.argv[3] == 'partial'){
  dbDumpFilePath          = backupConfigs.MONGO_PARTIAL_DUMP_FILE_FULL_PATH;
  out_encrypted_file      = backupConfigs.MONGO_PARTIAL_DUMP_FILENAME+'_'+file_date_ext+'.enc';
  out_encrypted_file_path = backupConfigs.MONGO_PARTIAL_DUMP_FILE_DIR+'/'+out_encrypted_file;
  args.push('--excludeCollection=status');
  args.push('--excludeCollection=useractivities');
  args.push('--excludeCollection=pushnotifies');
  args.push('--excludeCollection=importjobs');
  args.push('--excludeCollection=imports');
} else {
  dbDumpFilePath          = backupConfigs.MONGO_FULL_DUMP_FILE_FULL_PATH;
  out_encrypted_file      = backupConfigs.MONGO_FULL_DUMP_FILENAME+'_'+file_date_ext+'.enc';
  out_encrypted_file_path = backupConfigs.MONGO_FULL_DUMP_FILE_DIR+'/'+out_encrypted_file;
}       

args.push('--archive='+dbDumpFilePath);

console.log('\n');
console.log('[[[[[[   SCRIPT START DATETIME :  '+Date()+'   ]]]]]]');
console.log('[[[[[[   MongoDump Command Details   ]]]]]]');
console.log('\n');

var mongodump = spawn('/usr/bin/mongodump', args);
mongodump.stdout.on('data', function (data) {
  console.log('stdout: ' + data);
});
mongodump.stderr.on('data', function (data) {
  console.log('stderr: ' + data);
  MailClient.send('MongoDump Error : '+data);
});
mongodump.on('exit', function (code) {
  console.log('mongodump exited with code ' + code);
  if(code != 0){
    MailClient.send('MongoDump Error, process exited with Code : '+code);
    return;
  }
  encryptFile(dbDumpFilePath, out_encrypted_file_path, function(err,resp){
  	if(err){
			console.trace(err);
			console.log('\n');
			console.log('[[[[[[[[[   File Encryption has Failed   ]]]]]]]]]]');
      MailClient.send('MongoDump Error From EncryptFile: '+err);
			return;
		}
	  uploadFileToAws(out_encrypted_file_path, function(err,resp){
			if(err){
				console.trace(err);
				console.log('\n');
				console.log('[[[[[[[[[   AWS Upload has Failed   ]]]]]]]]]]');
				MailClient.send('MongoDump Error From AwsUpload : '+err);
        return;
			}
			deleteFile(dbDumpFilePath);
			console.log('[[[[[[[[[   AWS Upload has Succeeded   ]]]]]]]]]]');
      console.log('[[[[[[[[[[   SCRIPT END DATETIME  :  '+Date()+'   ]]]]]]]]]]');
		});
  });
});

function uploadFileToAws(filepath, cb){
  var params = {
    Bucket      : backupConfigs.BUCKET_NAME_DB_BACKUP, 
    Key         : out_encrypted_file,
    Body        : fs.createReadStream(filepath),
    ACL         : "authenticated-read"
  };
  s3.upload(params, function (err, data) {
    if(err){
      console.log('uploadFileToAws : Failed for input filepath : '+filepath);
      return cb(err,null);
    } 
    console.log(data);
    deleteFile(filepath);
    return cb(null,true);
  });
}

function deleteFile(filepath){
  fs.unlink(filepath,function(err,resp){
    if(err) {
      console.log('could not remove file : '+filepath+' , after upload to AWS');
      console.log('\n');
    } else {
      console.log('successfully deleted file : '+filepath+' , after upload to AWS');
      console.log('\n');
    }
  });
}

function encryptFile(inputFile, outputFile, cb){
	var modifiedKey = backupConfigs.ENCRYPTION_KEY+backupConfigs.ENCRYPTION_SALT;
	var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
	var cipher      = crypto.createCipher(backupConfigs.ALGORITHM, hashedKey); 
  var input       = fs.createReadStream(inputFile);
  var output      = fs.createWriteStream(outputFile);

	input.pipe(cipher).pipe(output);

	output.on('finish', function() {
	  console.log('Encrypted file written to disk!');
	  return cb(null,true);
	});
}