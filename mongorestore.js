var AWS                 = require('aws-sdk');
var crypto              = require('crypto');
var https               = require('https');
var fs                  = require('fs');

var backupConfigs       = require('./configs.js');
var MailClient          = require('./mail/mail_client.js');

AWS.config.update({
  accessKeyId     			: backupConfigs.AWS_ACCESS_KEY,
  secretAccessKey 			: backupConfigs.AWS_SECRET_KEY,
  "region"        			: backupConfigs.AWS_REGION,
  "output"        			: backupConfigs.AWS_OUTPUT 
});
var s3                  = new AWS.S3();

var DOWNLOAD_FILE_NAME        = process.argv[2];
if(!DOWNLOAD_FILE_NAME){
	console.log('[[[[[[[      NO DOWNLOAD FILENAME SPECIFIED     ]]]]]]]]');
	process.exit(0);
}

var current_date              = new Date();
var file_date_ext             = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate()+'_'+current_date.getHours()+'_'+current_date.getMinutes();
var in_encrypted_file_path    = backupConfigs.MONGO_RESTORE_FILE_DIR+'/'+backupConfigs.MONGO_RESTORE_FILE_NAME+'_'+file_date_ext+'.enc';
var out_decrypted_file_path   = backupConfigs.MONGO_RESTORE_FILE_DIR+'/'+backupConfigs.MONGO_RESTORE_FILE_NAME+'_'+file_date_ext+'.gz';

var params = {
  Bucket  : backupConfigs.BUCKET_NAME_DB_BACKUP,
  Key     : DOWNLOAD_FILE_NAME,
  Expires : backupConfigs.AMAZON_EXPIRY_TIME
};
s3.getSignedUrl('getObject', params, function (err, download_url) {
	if(err){
		console.log('[[[[[[[      AWS-S3 GET-SIGNED-URL FAILED     ]]]]]]]]');
		console.trace(err);
		process.exit(0);
	}
	if(!download_url){
		console.log('[[[[[[[     AWS-S3 GET-SIGNED-URL  : NO DOWNLOAD URL FOUND    ]]]]]]]]');
		process.exit(0);
	}
	var file = fs.createWriteStream(in_encrypted_file_path);
  https.get(download_url, function(response) {
  	console.log('[[[[[[     AWS-3 FILE DOWNLOAD RESPONSE CODE :  '+response.statusCode+'     ]]]]]]]');
  	if(response.statusCode != 200){
  		console.log('[[[[[[[    FAILURE IN DOWNLOADING FILE    ]]]]]]]');
  		process.exit(0);
  	}
    response.pipe(file);
  }).on('error', function(err) {
    console.log('[[[[[[[    ERROR IN DOWNLOADING FILE    ]]]]]]]');
    console.trace(err);
  });
  file.on('finish', function(){
  	console.log('[[[[[[[    WOW, DOWNLOADING FILE COMPLETED SUCCESSFULLY   ]]]]]]]');
  	console.log('[[[[[[[    NOW, DECRYPTION PROCEDURE WILL START    ]]]]]]]');
  	decryptFile(in_encrypted_file_path, out_decrypted_file_path, function(err,resp){
  		if(err){
  			console.log('[[[[[[[      FILE DECRYTPION FAILED     ]]]]]]]]');
  			console.trace(err);
  			process.exit(0);
  		}
  		console.log('[[[[[[[      FILE DECRYTPION SUCCESSFULLY COMPLETED     ]]]]]]]]');
  		console.log('[[[[[[[      DOWNLOADED DUMP-FILW WILL BE DELETED    ]]]]]]]]');
  		deleteFile(in_encrypted_file_path);
  	});
  });
});

function decryptFile(inputFile, outputFile, cb){
	var modifiedKey = backupConfigs.ENCRYPTION_KEY+backupConfigs.ENCRYPTION_SALT;
	var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
	var decipher    = crypto.createDecipher(backupConfigs.ALGORITHM, hashedKey); 
  var input       = fs.createReadStream(inputFile);
  var output      = fs.createWriteStream(outputFile);

	input.pipe(decipher).pipe(output);

	output.on('finish', function() {
	  console.log('[[[[[[[[     DECRYPTED FILE WRRITEN TO DISK     ]]]]]]]]]');
	  return cb(null,true)
	});
}

function deleteFile(filepath){
  fs.unlink(filepath,function(err,resp){
    if(err) {
      console.log('[[[[[[[ could not remove file : '+filepath+' , after upload to AWS  ]]]]]]]');
      console.log('\n');
    } else {
      console.log('[[[[[[[[    Successfully deleted file : '+filepath+' , after upload to AWS      ]]]]]]]');
      console.log('\n');
    }
    return;
  });
}