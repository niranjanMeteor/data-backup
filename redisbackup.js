require('shelljs/global');
var AWS                 = require('aws-sdk');
var fs                  = require('fs');
var backupConfigs       = require('./configs.js');
var crypto              = require('crypto');
var MailClient          = require('./mail/mail_client.js');

AWS.config.update({
  accessKeyId     			: backupConfigs.AWS_ACCESS_KEY,
  secretAccessKey 			: backupConfigs.AWS_SECRET_KEY,
  "region"        			: backupConfigs.AWS_REGION,
  "output"        			: backupConfigs.AWS_OUTPUT 
});
var s3                  = new AWS.S3();

var IDENTITY_FULL_FILE_PATH							= backupConfigs.IDENTITY_FULL_FILE_PATH;
var REDIS_SERVER_SSH_USER								= backupConfigs.REDIS_SERVER_SSH_USER;
var REDIS_SERVER_SSH_HOST								= backupConfigs.REDIS_SERVER_SSH_HOST;
var REDIS_SERVER_REMOTE_DUMP_FILE_PATH	= backupConfigs.REDIS_SERVER_REMOTE_DUMP_FILE_PATH;
var REDIS_SERVER_DUMP_DIR								= backupConfigs.REDIS_SERVER_DUMP_DIR;

var current_date                        = new Date();
var file_date_ext                       = current_date.getFullYear()+'_'+(current_date.getMonth()+1)+'_'+current_date.getDate()+'_'+current_date.getHours()+'_'+current_date.getMinutes();
var REDIS_SERVER_DUMP_FILE_PATH         = backupConfigs.REDIS_SERVER_DUMP_DIR+'/dump.rdb';
var REDIS_SERVER_DUMP_FILE_NAME_ENCRYPTED = 'redis_dump_'+file_date_ext+'.enc';
var REDIS_SERVER_DUMP_FILE_PATH_ENCRYPTED = backupConfigs.REDIS_SERVER_DUMP_DIR+'/'+REDIS_SERVER_DUMP_FILE_NAME_ENCRYPTED;


var SCP_COMMAND = 'scp -i '+IDENTITY_FULL_FILE_PATH+' '+REDIS_SERVER_SSH_USER+'@'+REDIS_SERVER_SSH_HOST+':'+REDIS_SERVER_REMOTE_DUMP_FILE_PATH+' '+REDIS_SERVER_DUMP_DIR;
console.log(SCP_COMMAND);
console.log('\n');
exec(SCP_COMMAND,function(code,stdout,stderr){
	if(stderr){
		console.log('[[[[[  CONNECTION TO SSH SERVER FAILED : STDERR ]]]]]]');
		console.log(stderr);
		console.log('[[[[[ EXITING THE SERVER ]]]]]]');
		MailClient.send('[[[[[  CONNECTION TO SSH SERVER FAILED : STDERR  = '+stderr+']]]]]]');
		return;
	}
	if(code != 0){
		console.log('[[[[[  CONNECTION TO SSH SERVER FAILED  ]]]]]]');
		console.log('[[[[[ OUTPUT CODE : '+code+' ]]]]]]');
		console.log('[[[[[ EXITING THE SERVER ]]]]]]');
		MailClient.send('[[[[[  CONNECTION TO SSH SERVER FAILED : CODE  = '+code+']]]]]]');
		return;
	}
	if(!stdout && isNaN(stdout)){
		console.log('[[[[[  CONNECTION TO SSH SERVER FAILED : STDOUT ]]]]]]');
		console.log(stdout);
		console.log('[[[[[ EXITING THE SERVER ]]]]]]');
		MailClient.send('[[[[[  CONNECTION TO SSH SERVER FAILED : STDOUT  = '+stdout+']]]]]]');
		return;
	}
	encryptFile(REDIS_SERVER_DUMP_FILE_PATH, REDIS_SERVER_DUMP_FILE_PATH_ENCRYPTED, function(err,resp){
  	if(err){
			console.trace(err);
			console.log('\n');
			console.log('[[[[[[[[[   File Encryption has Failed   ]]]]]]]]]]');
      MailClient.send('RedisDump Error From EncryptFile: '+err);
			return;
		}
	  uploadFileToAws(REDIS_SERVER_DUMP_FILE_PATH_ENCRYPTED, function(err,resp){
			if(err){
				console.trace(err);
				console.log('\n');
				console.log('[[[[[[[[[   AWS Upload has Failed   ]]]]]]]]]]');
				MailClient.send('RedisDump Error From AwsUpload : '+err);
        return;
			}
			deleteFile(REDIS_SERVER_DUMP_FILE_PATH);
			console.log('[[[[[[[[[   AWS Upload has Succeeded   ]]]]]]]]]]');
      console.log('[[[[[[[[[[   SCRIPT END DATETIME  :  '+Date()+'   ]]]]]]]]]]');
		});
  });
});

function uploadFileToAws(filepath, cb){
  var params = {
    Bucket      : backupConfigs.BUCKET_NAME_DB_BACKUP, 
    Key         : REDIS_SERVER_DUMP_FILE_NAME_ENCRYPTED,
    Body        : fs.createReadStream(filepath),
    ACL         : "authenticated-read"
  };
  s3.upload(params, function (err, data) {
    if(err){
      console.log('uploadFileToAws : Failed for input filepath : '+filepath);
      return cb(err,null);
    } 
    console.log(data);
    deleteFile(filepath);
    return cb(null,true);
  });
}

function deleteFile(filepath){
  fs.unlink(filepath,function(err,resp){
    if(err) {
      console.log('could not remove file : '+filepath+' , after upload to AWS');
      console.log('\n');
    } else {
      console.log('successfully deleted file : '+filepath+' , after upload to AWS');
      console.log('\n');
    }
  });
}

function encryptFile(inputFile, outputFile, cb){
	var modifiedKey = backupConfigs.ENCRYPTION_KEY+backupConfigs.ENCRYPTION_SALT;
	var hashedKey   = crypto.createHash('sha256').update(modifiedKey).digest('base64');
	var cipher      = crypto.createCipher(backupConfigs.ALGORITHM, hashedKey); 
  var input       = fs.createReadStream(inputFile);
  var output      = fs.createWriteStream(outputFile);

	input.pipe(cipher).pipe(output);

	output.on('finish', function() {
	  console.log('Encrypted file written to disk!');
	  return cb(null,true);
	});
}